Clipper HEAD is found in DRUPAL-5 Branch.
Please commit your patches against that branch.

If you are interested in development on the HEAD branch, or DRUPAL-6, please email the maintainer (ber).